const form = document.querySelector('form');
const proxy = document.querySelector('#proxy');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  const url = form.elements.url.value;
  proxy.setAttribute('src', `https://api.allorigins.win/raw?url=${url}`);
  proxy.style.display = 'block';
});
